mod input;

mod shapes;
use shapes::hexagon::Hexagon;

use bevy::{
    core::FixedTimestep, prelude::*, render::camera::PerspectiveProjection,
};
use bevy_easings::{
    custom_ease_system, CustomComponentEase, Ease, EaseFunction,
    EasingType, EasingsPlugin, Lerp,
};
use bevy_math::Vec3;

use std::{collections::HashMap, fmt::Debug, time::Duration};

const HEXAGON_SIZE: f32 = 2.5;
const HEXAGON_SPACE: f32 = 0.2;
const FADE_TIME: u64 = 800;

#[derive(Debug, Default)]
struct MyEaseValue<T>(pub T);

impl Lerp for MyEaseValue<Color> {
    type Scalar = f32;

    fn lerp(&self, other: &Self, scalar: &Self::Scalar) -> Self {
        MyEaseValue(self.0 + (other.0 + (self.0 * -1.)) * *scalar)
    }
}

#[derive(Debug, Clone)]
struct HexGridCell {
    x: i32,
    y: i32,
    color: TileColor,
}

impl HexGridCell {
    /// Returns the coordinates of the center of the hexagon cell in world
    /// coordinates.
    pub(crate) fn center(&self, size: f32) -> Vec3 {
        center_of_hex_cell(self.x, self.y, size)
    }
}

/// Returns the coordinates of the center of a hexagon cell in world
/// coordinates.
pub(crate) fn center_of_hex_cell(cell_x: i32, cell_y: i32, size: f32) -> Vec3 {
    let cx = cell_x as f32;
    let cy = cell_y as f32;
    let x = size * 3.0f32.sqrt() * (cx + 0.5 * ((cell_y & 1) as f32));
    let z = size * 1.5 * cy;
    Vec3::new(x, 0., z)
}

#[derive(Debug)]
struct HexTileDirections(Vec<Vec<HexDirection>>);

#[derive(Debug, Clone)]
struct HexGrid(HashMap<(i32, i32), TileColor>);

impl HexGrid {
    fn new(directions: &HexTileDirections) -> HexGrid {
        let mut hm = HashMap::new();

        let coords = directions
            .0
            .iter()
            .map(|l| l.iter().fold((0, 0), |(x, y), dir| dir.step(x, y)))
            .collect::<Vec<_>>();

        for l in directions.0.iter() {
            let mut pos = (0, 0);
            for dir in l {
                if !hm.contains_key(&pos) {
                    hm.insert(pos, TileColor::White);
                }
                pos = dir.step(pos.0, pos.1);
            }
        }

        for &pos in &coords {
            hm.insert(pos, TileColor::Black);
        }
        HexGrid(hm)
    }

    fn black_neighbor_count(&self, x: i32, y: i32) -> usize {
        use HexDirection::*;
        use TileColor::*;

        [E, SE, SW, W, NW, NE]
            .iter()
            .filter(|dir| self.0.get(&dir.step(x, y)) == Some(&Black))
            .count()
    }
}

#[derive(Debug)]
enum HexDirection {
    E,
    SE,
    SW,
    W,
    NW,
    NE,
}

impl HexDirection {
    fn step(&self, x: i32, y: i32) -> (i32, i32) {
        match self {
            HexDirection::E => (x + 1, y),
            HexDirection::SE => (if y % 2 == 0 { x } else { x + 1 }, y + 1),
            HexDirection::SW => (if y % 2 == 0 { x - 1 } else { x }, y + 1),
            HexDirection::W => (x - 1, y),
            HexDirection::NW => (if y % 2 == 0 { x - 1 } else { x }, y - 1),
            HexDirection::NE => (if y % 2 == 0 { x } else { x + 1 }, y - 1),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum TileColor {
    White,
    Black,
}

#[derive(Debug)]
struct Colors {
    background: Color,
    white: Color,
    black: Color,
}

#[derive(Debug, Default)]
struct EvolutionState {
    generation_counter: u32,
}

#[derive(Debug, Default)]
struct RotationParams {
    phase1: f32,
    phase2: f32,
}

#[derive(Debug)]
struct HexRoot;

#[derive(Debug)]
struct Fade;

fn main() {
    let mut app = App::build();
    app.add_resource(Msaa { samples: 4 })
        .add_resource(HexGrid::new(&parse_input(input::INPUT)))
        .add_resource(ClearColor(Color::hex("192a56").unwrap()))
        .add_plugins(DefaultPlugins)
        .add_plugin(EasingsPlugin);

    #[cfg(target_arch = "wasm32")]
    app.add_plugin(bevy_webgl2::WebGL2Plugin);

    app.add_startup_system(setup.system())
        .add_startup_stage("grid_setup", SystemStage::serial())
        .add_startup_system_to_stage(
            "grid_setup",
            create_initial_grid_cells.system(),
        )
        .add_system(cell_position_system.system())
        .add_stage_after(
            stage::UPDATE,
            "evolve",
            SystemStage::parallel()
                .with_run_criteria(FixedTimestep::step(4.0))
                .with_system(evolve_grid.system()),
        )
        .add_system(fade_cells.system())
        .add_system(custom_ease_system::<MyEaseValue<Color>>.system())
        .add_system(rotate_grid.system())
        .run();
}

fn setup(commands: &mut Commands) {
    let camera_dist = 150.0;

    commands
        .insert_resource(Colors {
            background: Color::hex("192a5600").unwrap(),
            white: Color::hex("ffffff").unwrap(),
            black: Color::hex("000000").unwrap(),
        })
        .spawn(LightBundle {
            transform: Transform::from_translation(Vec3::new(-4.0, 7.0, -4.0)),
            ..Default::default()
        })
        .spawn(LightBundle {
            transform: Transform::from_translation(Vec3::new(4.0, 8.0, 4.0)),
            ..Default::default()
        })
        .spawn(LightBundle {
            transform: Transform::from_translation(Vec3::new(1.0, 8.0, 10.0)),
            ..Default::default()
        })
        .spawn(Camera3dBundle {
            transform: Transform::from_translation(Vec3::new(
                camera_dist * 0.5,
                camera_dist,
                camera_dist * 0.75,
            ))
            .looking_at(Vec3::default(), Vec3::unit_y()),
            perspective_projection: PerspectiveProjection {
                far: 1000000.0,
                ..Default::default()
            },
            ..Default::default()
        });
}

fn create_initial_grid_cells(
    commands: &mut Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    colors: Res<Colors>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    grid: Res<HexGrid>,
) {
    let parent = commands
        .spawn((HexRoot, GlobalTransform::default(), Transform::default()))
        .current_entity()
        .unwrap();

    for (&(x, y), &color) in grid.0.iter() {
        spawn_hex_cell(
            parent,
            commands,
            &mut meshes,
            &mut materials,
            x,
            y,
            color,
            match color {
                TileColor::White => colors.white,
                TileColor::Black => colors.black,
            },
        );
    }
}

fn parse_input(input: &str) -> HexTileDirections {
    use HexDirection::*;

    let mut directions = Vec::new();

    for l in input.lines() {
        let mut row = Vec::new();
        let mut iter = l.chars().peekable();
        while let Some(ch) = iter.next() {
            let dir = match ch {
                'e' => E,
                's' if iter.peek() == Some(&'e') => {
                    iter.next();
                    SE
                }
                's' if iter.peek() == Some(&'w') => {
                    iter.next();
                    SW
                }
                'w' => W,
                'n' if iter.peek() == Some(&'w') => {
                    iter.next();
                    NW
                }
                'n' if iter.peek() == Some(&'e') => {
                    iter.next();
                    NE
                }
                _ => unreachable!("unexpected char: {}", ch),
            };
            row.push(dir);
        }
        directions.push(row);
    }

    HexTileDirections(directions)
}

// Translate each cell in the grid.
fn cell_position_system(mut query: Query<(&HexGridCell, &mut Transform)>) {
    for (cell, mut transform) in query.iter_mut() {
        *transform = Transform::from_translation(
            cell.center(HEXAGON_SIZE + HEXAGON_SPACE),
        );
    }
}

fn evolve_grid(
    commands: &mut Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    colors: Res<Colors>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    mut grid: ResMut<HexGrid>,
    hex_root_query: Query<(Entity, &HexRoot)>,
    mut cells_query: Query<(
        Entity,
        &mut HexGridCell,
    )>,
    mut camera_query: Query<(Entity, &PerspectiveProjection, &mut Transform)>,
    mut evolution_state: Local<EvolutionState>,
) {
    let parent = hex_root_query.iter().nth(0).unwrap().0;

    use TileColor::*;

    let mut new_grid = grid.clone();

    let x1 = grid.0.keys().min_by_key(|(x, _)| x).unwrap().0 - 1;
    let x2 = grid.0.keys().max_by_key(|(x, _)| x).unwrap().0 + 1;
    let y1 = grid.0.keys().min_by_key(|(_, y)| y).unwrap().1 - 1;
    let y2 = grid.0.keys().max_by_key(|(_, y)| y).unwrap().1 + 1;

    for x in x1..=x2 {
        for y in y1..=y2 {
            let bnc = grid.black_neighbor_count(x, y);
            match grid.0.get(&(x, y)) {
                Some(&Black) if bnc == 0 || bnc > 2 => {
                    new_grid.0.insert((x, y), White);
                    for (entity, mut cell) in cells_query.iter_mut() {
                        if cell.x == x && cell.y == y {
                            cell.color = White;
                            fade_to(
                                commands,
                                entity,
                                colors.black,
                                colors.white,
                                FADE_TIME,
                            );
                            break;
                        }
                    }
                }
                Some(&White) if bnc == 2 => {
                    new_grid.0.insert((x, y), Black);
                    for (entity, mut cell) in cells_query.iter_mut() {
                        if cell.x == x && cell.y == y {
                            cell.color = Black;
                            fade_to(
                                commands,
                                entity,
                                colors.white,
                                colors.black,
                                FADE_TIME,
                            );
                            break;
                        }
                    }
                }
                None if bnc == 2 => {
                    new_grid.0.insert((x, y), Black);
                    let entity = spawn_hex_cell(
                        parent,
                        commands,
                        &mut meshes,
                        &mut materials,
                        x,
                        y,
                        Black,
                        colors.background,
                    );
                    fade_to(
                        commands,
                        entity,
                        colors.background,
                        colors.black,
                        FADE_TIME,
                    );
                }
                _ => {}
            }
        }
    }

    *grid = new_grid;

    let mut cells: Vec<HexGridCell> = Vec::new();

    for (_, cell) in cells_query.iter_mut() {
        cells.push(cell.clone());
    }

    if evolution_state.generation_counter % 4 == 0 {
        update_camera_zoom(commands, &mut camera_query, &cells);
    }

    evolution_state.generation_counter += 1;
}

fn fade_to(
    commands: &mut Commands,
    entity: Entity,
    from: Color,
    to: Color,
    millis: u64,
) {
    commands.set_current_entity(entity);
    commands.with(Fade).with(MyEaseValue(from)).with(
        MyEaseValue(from).ease_to(
            MyEaseValue(to),
            EaseFunction::QuadraticOut,
            EasingType::Once {
                duration: Duration::from_millis(millis),
            },
        ),
    );
}

fn spawn_hex_cell(
    parent: Entity,
    commands: &mut Commands,
    meshes: &mut ResMut<Assets<Mesh>>,
    materials: &mut ResMut<Assets<StandardMaterial>>,
    x: i32,
    y: i32,
    tile_color: TileColor,
    color: Color,
) -> Entity {
    commands
        .spawn(PbrBundle {
            mesh: meshes.add(Mesh::from(Hexagon { size: HEXAGON_SIZE })),
            material: materials.add(color.into()),
            transform: Transform::from_translation(center_of_hex_cell(
                x,
                y,
                HEXAGON_SIZE + HEXAGON_SPACE,
            )),
            ..Default::default()
        })
        .with(HexGridCell {
            x,
            y,
            color: tile_color,
        })
        .with(Parent(parent))
        .current_entity()
        .unwrap()
}

fn update_camera_zoom(
    commands: &mut Commands,
    camera_query: &mut Query<(Entity, &PerspectiveProjection, &mut Transform)>,
    cells: &[HexGridCell],
) {
    for (entity, proj, transform) in camera_query.iter_mut() {
        // let view_plane_normal = transform.forward();

        let cell_x_min = cells.iter().map(|c| c.x).min().unwrap();
        let cell_x_max = cells.iter().map(|c| c.x).max().unwrap();
        let cell_y_min = cells.iter().map(|c| c.y).min().unwrap();
        let cell_y_max = cells.iter().map(|c| c.y).max().unwrap();
        let x_min =
            center_of_hex_cell(cell_x_min, 0, HEXAGON_SIZE + HEXAGON_SPACE).x;
        let x_max =
            center_of_hex_cell(cell_x_max, 0, HEXAGON_SIZE + HEXAGON_SPACE).x;
        let y_min =
            center_of_hex_cell(0, cell_y_min, HEXAGON_SIZE + HEXAGON_SPACE).y;
        let y_max =
            center_of_hex_cell(0, cell_y_max, HEXAGON_SIZE + HEXAGON_SPACE).y;
        let cx = (x_max + x_min) / 2.;
        let cy = (y_max + y_min) / 2.;
        let dx = x_max - x_min + (HEXAGON_SIZE + HEXAGON_SPACE) * 2.;
        let dy = y_max - y_min + (HEXAGON_SIZE + HEXAGON_SPACE) * 2.;
        let s = dx.max(dy);
        let camera_dist = s / 2.0 / (proj.fov / 2.).tan();

        let easing_component = transform.ease_to(
            Transform::from_translation(Vec3::new(
                camera_dist * 0.5,
                camera_dist,
                camera_dist * 0.75,
            ))
            .looking_at(Vec3::new(cx, 0., cy), Vec3::unit_y()),
            EaseFunction::QuadraticInOut,
            EasingType::Once {
                duration: Duration::from_secs(4),
            },
        );

        commands.insert_one(entity, easing_component);
    }
}

fn rotate_grid(
    mut query: Query<(Entity, &HexRoot, &mut Transform)>,
    mut rotation_params: Local<RotationParams>,
) {
    rotation_params.phase1 += 0.01;
    rotation_params.phase2 += 0.0075;
    let a1 = rotation_params.phase1;
    let a2 = rotation_params.phase2;
    let angle = a1.sin() * a2.sin() * 0.01;
    for (_, _, mut transform) in query.iter_mut() {
        transform.rotate(Quat::from_rotation_y(angle));
    }
}

fn fade_cells(
    mut materials: ResMut<Assets<StandardMaterial>>,
    mut query: Query<
        (&MyEaseValue<Color>, &Handle<StandardMaterial>),
        With<Fade>,
    >,
) {
    for (color, mat_handle) in query.iter_mut() {
        materials.set(mat_handle, color.0.into());
    }
}
