use bevy::{
    prelude::*,
    render::{mesh::Indices, pipeline::PrimitiveTopology},
};

use std::f32::consts::PI;

/// A hexagon on the XZ plane.
#[derive(Debug)]
pub struct Hexagon {
    pub size: f32,
}

impl From<Hexagon> for Mesh {
    fn from(hexagon: Hexagon) -> Self {
        let normal = [0.0, 1.0, 0.0];
        let uv = [0.0, 0.0];

        let indices = Indices::U32(vec![0, 5, 1, 1, 4, 2, 2, 4, 3, 1, 5, 4]);

        let mut positions = Vec::new();
        let mut normals = Vec::new();
        let mut uvs = Vec::new();

        for i in 0..6 {
            positions.push(hexagon.corner(i));
            normals.push(normal);
            uvs.push(uv);
        }

        let mut mesh = Mesh::new(PrimitiveTopology::TriangleList);
        mesh.set_indices(Some(indices));
        mesh.set_attribute(Mesh::ATTRIBUTE_POSITION, positions);
        mesh.set_attribute(Mesh::ATTRIBUTE_NORMAL, normals);
        mesh.set_attribute(Mesh::ATTRIBUTE_UV_0, uvs);
        mesh
    }
}

impl Hexagon {
    fn corner(&self, i: u32) -> [f32; 3] {
        let angle_deg = 60. * i as f32 - 30.;
        let angle_rad = PI / 180. * angle_deg;
        [self.size * angle_rad.cos(), 0., self.size * angle_rad.sin()]
    }
}
